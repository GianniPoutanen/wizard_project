﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonderingEnemy : MonoBehaviour
{
    // Movement Variables
    private Vector2 prevPos;
    public float distanceTravelled = 0;
    private Vector3 direction = new Vector3();
    // Wondering Variables
    private bool wondering = true;
    private bool newWonderingDirection = true;
    private float wonderingWaitTime = 1;
    private float wonderingWaitTimer;
    public float wonderingSpeed;
    // Stalking Variables - If not wondering then probably stalking
    public float viewRange = 3;
    private bool stalking = false;
    public float stalkingSpeed = 0.04f;
    private Transform Target;
    private Vector3 investigatePosition;

    // Start is called before the first frame update
    void Start()
    {
        prevPos = this.transform.position;
        wonderingWaitTimer = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        investigatePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (wondering && !stalking)
        {
            Wondering();
            stalking = CheckPlayerInRange();
        }
        else if (stalking)
        {
            if (0 < Vector2.Distance(this.transform.position, investigatePosition))
            {
                Stalking();
                stalking = CheckPlayerInRange();
            }
            else
            {
                stalking = CheckPlayerInRange();
                investigatePosition = Vector2.negativeInfinity;
                wondering = true;
                wonderingWaitTimer = Time.time + wonderingWaitTime + Random.Range(2f, 4f);
            }
        }

        Debug.Log(stalking);
    }

    /// <summary>
    /// Wondering around randomly
    /// </summary>
    public void Stalking()
    {
        // save the current rotation
        Quaternion rotation = transform.rotation;
        // Spin it around a random amount
        transform.rotation = GetRotationTowardsMouse();
        // move it a random amount forward based on it current facing
        direction = transform.right;
        // put it back to its original rotation
        transform.rotation = rotation;

        this.transform.position += direction * stalkingSpeed;
        distanceTravelled -= Vector2.Distance(this.transform.position, prevPos);
        prevPos = this.transform.position;
    }

    /// <summary>
    /// Wondering around randomly
    /// </summary>
    public void Wondering()
    {
        if (newWonderingDirection)
        {
            if (wonderingWaitTimer < Time.time)
            {
                // save the current rotation
                Quaternion rotation = transform.rotation;
                // Spin it around a random amount
                transform.Rotate(Vector3.forward, Random.Range(0, 360));
                // move it a random amount forward based on it current facing
                direction = transform.right;
                // put it back to its original rotation
                transform.rotation = rotation;
                prevPos = this.transform.position;
                newWonderingDirection = false;
                distanceTravelled = Random.Range(0.5f, 4f);
            }
        }
        if (!newWonderingDirection)
        {
            // Wondering now
            if (distanceTravelled > 0)
            {
                this.transform.position += direction * wonderingSpeed;
                distanceTravelled -= Vector2.Distance(this.transform.position, prevPos);
                prevPos = this.transform.position;
            }
            else
            {
                wonderingWaitTimer = Time.time + wonderingWaitTime;
                newWonderingDirection = true;
            }
        }
    }

    private bool CheckPlayerInRange()
    {
        return 3 > Vector2.Distance(Camera.main.ScreenToWorldPoint(Input.mousePosition), this.transform.position);
    }
    private Quaternion GetRotationTowardsMouse()
    {
        Vector3 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
