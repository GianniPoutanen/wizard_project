﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    public Vector3 savedRotation;
    public bool movingToNext;
    public float moveToNextSpeed;
    public SpellSegment currentSegment;
    private float deathTimer;
    private float totalDistance;
    private Vector3 lastPosition;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = this.transform.position;
        currentSegment.SpellTransitionEffect(this.gameObject);
        this.GetComponent<Animator>().Play("FireBallAnimation", -1, Random.Range(0.0f, 1.0f));
        this.transform.localScale = new Vector3(currentSegment.spellStats.size, currentSegment.spellStats.size, currentSegment.spellStats.size);
        this.GetComponent<Animator>().speed = 1 + currentSegment.spellStats.speed / 5;
        if (!currentSegment.spellNextPosition.Equals(Vector3.negativeInfinity))
        {
            this.savedRotation = this.transform.eulerAngles;
            Quaternion rotation = Quaternion.LookRotation(currentSegment.spellNextPosition - transform.position, transform.TransformDirection(Vector3.up));
            transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
            movingToNext = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentSegment.SpellEffectStep();

        if (totalDistance > (currentSegment.spellStats.range * currentSegment.rangeRatio))
        {
            currentSegment = currentSegment.SpellFinisher();
            if (currentSegment == null)
            {
                Destroy(this.gameObject);
                return;
            }
            totalDistance = 0;
        }
        if (movingToNext)
        {
            if (Vector2.Distance(this.transform.position, currentSegment.spellNextPosition) > currentSegment.spellStats.speed)
            {
                transform.position += transform.right * Time.deltaTime * currentSegment.spellStats.speed;
            }
            else
            {
                transform.position = currentSegment.spellNextPosition;
                movingToNext = false;
            }
        }
        else
        {
            transform.position += transform.right * Time.deltaTime * currentSegment.spellStats.speed;
        }

        // update distance travelled
        float distance = Vector3.Distance(lastPosition, transform.position);
        totalDistance += distance;
        lastPosition = transform.position;
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
