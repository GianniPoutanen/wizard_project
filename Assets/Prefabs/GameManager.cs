﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject inventory;
    public SpellEmitter spellEmitter;
    public bool pause;

    public void Start()
    {
        if (spellEmitter == null)
        {
            spellEmitter = GameObject.FindGameObjectWithTag("SpellEmitter").GetComponent<SpellEmitter>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (inventory.activeSelf)
            {
                spellEmitter.UpdateSpell(inventory.GetComponent<InventoryManager>().starterSegments);
            }
            inventory.SetActive(!inventory.activeSelf);
            pause = !pause;
        }
    }
}
