﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    private GameManager gm;

    [Header("Tablet slot variables")]
    public int height = 3;
    public int width = 3;
    public int tabletSlotOffset = 4;
    public GameObject[,] tabletSlots;
    public GameObject[,] tablet;
    [Header("Inventory slot variables")]
    public int numInvSlots = 3;
    public int invSlotOffset = 4;
    public GameObject[] inventorySlots;
    public GameObject[] inventory;
    [Header("Inventory slot variables")]
    public List<SpellSegment> starterSegments = new List<SpellSegment>();
    public int allowedStarters = 1;

    public Item draggedItem = null;

    public GameObject itemObject;


    [Header("Inventory builder variables")]
    public GameObject invenetorySlotImage;
    public GameObject tabletSlotImage;
    public bool renderUpdate = true;



    // Start is called before the first frame update
    void Start()
    {
        tabletSlots = new GameObject[width, height];
        tablet = new GameObject[width, height];
        inventorySlots = new GameObject[numInvSlots];
        inventory = new GameObject[numInvSlots];
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inventorySlots.Length != numInvSlots || tabletSlots.GetLength(0) != width || tabletSlots.GetLength(1) != height)
        {
            tabletSlots = new GameObject[width, height];
            tablet = new GameObject[width, height];
            inventorySlots = new GameObject[numInvSlots];
            inventory = new GameObject[numInvSlots];
            renderUpdate = true;
        }

        if (renderUpdate)
        {
            foreach (Transform child in this.transform)
            {
                Destroy(child.gameObject);
            }

            for (int i = 0; i < numInvSlots; i++)
            {
                GameObject invSlot = Instantiate(invenetorySlotImage);
                invSlot.transform.SetParent(this.transform);
                RectTransform rectPos = invSlot.GetComponent<RectTransform>();
                rectPos.anchoredPosition3D = new Vector3(-((float)numInvSlots / 2.0f * (rectPos.sizeDelta.x + invSlotOffset)) +
                                                          ((rectPos.sizeDelta.x + invSlotOffset) * i) + (rectPos.sizeDelta.x / 2) + (invSlotOffset / 2.0f), 70, -1);
                invSlot.GetComponent<Slot>().slotPosition = new int[] { i };
                invSlot.GetComponent<Slot>().isInventory = true;
                invSlot.tag = "Inventory";
                invSlot.name = "Inventory: " + i;
                inventorySlots[i] = invSlot;
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    GameObject tabSlot = Instantiate(tabletSlotImage);
                    tabSlot.transform.SetParent(this.transform);
                    tabSlot.tag = "Tablet";
                    tabSlot.name = "Tablet: " + i + " and " + j;
                    RectTransform rectPos = tabSlot.GetComponent<RectTransform>();
                    rectPos.anchoredPosition3D = new Vector3(-((float)width / 2.0f * (rectPos.sizeDelta.x + tabletSlotOffset)) +
                                                              ((rectPos.sizeDelta.x + tabletSlotOffset) * i) + (rectPos.sizeDelta.x / 2) + (invSlotOffset / 2.0f),
                                                              180 - ((float)height / 2.0f * (rectPos.sizeDelta.y + tabletSlotOffset)) +
                                                              ((rectPos.sizeDelta.y + tabletSlotOffset) * j) + (rectPos.sizeDelta.y / 2) + (invSlotOffset / 2.0f)
                                                              , -1);

                    tabSlot.GetComponent<Slot>().slotPosition = new int[] { i, j };
                    tabletSlots[i, j] = tabSlot;
                }
            }

            AddTestItems();
            renderUpdate = false;
        }
    }

    public bool ValidatePlacement(Item item, int[] position)
    {
        return true;
    }

    public void AddTestItems()
    {
        ItemsLibrary newLib = new ItemsLibrary();
        newLib.PopulateSegments();
        newLib.PopulateStarters();

        for (int i = 0; i < (inventorySlots.Length / 2); i++)
        {
            GameObject item = Instantiate(itemObject);
            item.transform.SetParent(this.transform);
            Slot invSlot = this.GetNextInventory();
            item.GetComponent<Item>().currentPosition = invSlot.GetComponent<RectTransform>().anchoredPosition;
            item.GetComponent<Item>().anchorPoint = invSlot.GetComponent<RectTransform>().anchoredPosition;
            item.GetComponent<Item>().shrunk = true;
            item.GetComponent<Item>().occupiedSlots.Add(invSlot);
            item.GetComponent<Item>().spellSegment = newLib.GenerateRandomBasicSegment(15);//newLib.availableSegments[i + 1];;
            item.GetComponent<Item>().ShrinkForInventory();
            invSlot.isOccupied = true;
        }
        List<SpellSegment> spellSegments = newLib.GenerateAllKeyWordSegents();
        for (int i = 0; i < spellSegments.Count; i++)
        {
            GameObject item = Instantiate(itemObject);
            item.transform.SetParent(this.transform);
            Slot invSlot = this.GetNextInventory();
            item.GetComponent<Item>().currentPosition = invSlot.GetComponent<RectTransform>().anchoredPosition;
            item.GetComponent<Item>().anchorPoint = invSlot.GetComponent<RectTransform>().anchoredPosition;
            item.GetComponent<Item>().shrunk = true;
            item.GetComponent<Item>().occupiedSlots.Add(invSlot);
            item.GetComponent<Item>().spellSegment = spellSegments[i];

            item.GetComponent<Item>().ShrinkForInventory();
            invSlot.isOccupied = true;
        }

        GameObject starterSlot = Instantiate(itemObject);
        starterSlot.transform.SetParent(this.transform);
        Slot invSlotTemp = this.GetNextInventory();
        starterSlot.GetComponent<Item>().currentPosition = invSlotTemp.GetComponent<RectTransform>().anchoredPosition;
        starterSlot.GetComponent<Item>().anchorPoint = invSlotTemp.GetComponent<RectTransform>().anchoredPosition;
        starterSlot.GetComponent<Item>().shrunk = true;
        starterSlot.GetComponent<Item>().occupiedSlots.Add(invSlotTemp);
        starterSlot.GetComponent<Item>().spellSegment = newLib.availableStarters[0];
        starterSlot.GetComponent<Item>().ShrinkForInventory();
        invSlotTemp.isOccupied = true;
    }

    public bool CheckSlotsFree(List<Slot> slots)
    {
        foreach (Slot slot in slots)
        {
            if (slot.isOccupied)
            {
                return false;
            }
        }
        return true;
    }

    public Slot GetNextInventory()
    {
        foreach (GameObject slot in inventorySlots)
        {
            if (!slot.GetComponent<Slot>().isOccupied)
            {
                return slot.GetComponent<Slot>();
            }
        }
        return null;
    }
}
