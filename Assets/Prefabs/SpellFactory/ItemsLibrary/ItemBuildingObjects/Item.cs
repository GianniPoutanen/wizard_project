﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IEndDragHandler, IDragHandler, IBeginDragHandler
{
    private Vector3 vectorOffSet;
    private bool dragging = false; // true when being dragged
    public bool shrunk = true; // true when being dragged
    private int rotating = 0;
    private List<GameObject> solidBlockObjects = new List<GameObject>();
    private Dictionary<GameObject, SpellSegment.Segment> inputObjectPairs = new Dictionary<GameObject, SpellSegment.Segment>();
    private List<GameObject> outputObjects = new List<GameObject>();
    public List<Slot> occupiedSlots = new List<Slot>();
    public List<Item> outputSegments = new List<Item>();
    public List<Item> inputSegments = new List<Item>();

    [Header("Position of item variables")]
    public Vector2 anchorPoint;
    public Vector2 currentPosition;
    public Vector2 dragPosition;
    [Header("Manager variables")]
    public InventoryManager inventoryManager;
    private CanvasGroup canvasGroup;

    [Header("Spell building variables")]
    public SpellSegment spellSegment = new SpellSegment();
    public GameObject inputOutputImage;
    public GameObject solidImage;

    public void OnBeginDrag(PointerEventData eventData)
    {
        dragging = true;
        RemoveFromInputSegments();

        // reset slots under pieces
        inventoryManager.draggedItem = this;
        canvasGroup.blocksRaycasts = false;
        foreach (Transform child in this.transform)
        {
            if (child.tag == "Item")
                child.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
        dragPosition = Input.mousePosition;
        this.transform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        List<RaycastResult> castResults = RaycastMouse();
        if (castResults.Count > 0)
        {
            foreach (RaycastResult rayCast in castResults)
            {
                if (rayCast.gameObject.tag == "Inventory")
                {
                    if (Vector3.Distance(rayCast.gameObject.transform.position, Input.mousePosition) < Mathf.Abs((rayCast.gameObject.transform as RectTransform).rect.x / 1.3f))
                    {
                        this.ShrinkForInventory();
                        dragPosition = rayCast.gameObject.transform.position;
                    }
                }
                else
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                    Vector2 tabletSlotOffset = CheckSlotPositions();
                    List<Slot> slots = GetSlotsUnderSegment();
                    if (CheckInputSlots() && !tabletSlotOffset.Equals(Vector2.negativeInfinity) && inventoryManager.CheckSlotsFree(slots)
                        && Vector2.Distance(Input.mousePosition, this.transform.position) < 15)
                    {
                        // slot into position
                        dragPosition = (Vector2)this.transform.position - tabletSlotOffset;
                        // (new Vector2(15 * Mathf.Sign(tabletSlotOffset.x), 15 * Mathf.Sign(tabletSlotOffset.y)) * ((spellSegment.shape.GetLength(0) - 3) % 2));
                    }
                    else
                    {
                        this.transform.localScale = new Vector3(1, 1, 1);
                        dragPosition = Input.mousePosition;
                    }
                }
            }
        }
        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
            dragPosition = Input.mousePosition;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Vector2 pos = CheckSlotPositions();
        if (CheckInputSlots() && !pos.Equals(Vector2.negativeInfinity))
        {
            //this.transform.position += (Vector3)pos;
            List<Slot> slots = GetSlotsUnderSegment();
            if (inventoryManager.CheckSlotsFree(slots))
            {
                if (this.inputObjectPairs.Count == 0 && this.inventoryManager.starterSegments.Count < this.inventoryManager.allowedStarters)
                {
                    Vector2 tabletSlotOffset = CheckSlotPositions();
                    if (CheckInputSlots() && !tabletSlotOffset.Equals(Vector2.negativeInfinity) && inventoryManager.CheckSlotsFree(slots)
                        && Vector2.Distance(Input.mousePosition, this.transform.position) < 15)
                    {
                        // slot into position
                        dragPosition = (Vector2)this.transform.position - tabletSlotOffset;
                        // (new Vector2(15 * Mathf.Sign(tabletSlotOffset.x), 15 * Mathf.Sign(tabletSlotOffset.y)) * ((spellSegment.shape.GetLength(0) - 3) % 2));
                    }
                    occupiedSlots = slots;
                    anchorPoint = this.GetComponent<RectTransform>().anchoredPosition;
                    this.shrunk = false;
                    inventoryManager.starterSegments.Add(this.spellSegment);
                }
                else if (this.inputObjectPairs.Count == 0 && this.inventoryManager.starterSegments.Count == this.inventoryManager.allowedStarters)
                {
                    Slot invSlot = inventoryManager.GetNextInventory();
                    if (invSlot != null)
                    {
                        invSlot.isOccupied = true;
                        occupiedSlots.Add(invSlot);
                        anchorPoint = invSlot.GetComponent<RectTransform>().anchoredPosition;
                    }
                }
                else
                {
                    Vector2 tabletSlotOffset = CheckSlotPositions();
                    if (CheckInputSlots() && !tabletSlotOffset.Equals(Vector2.negativeInfinity) && inventoryManager.CheckSlotsFree(slots)
                        && Vector2.Distance(Input.mousePosition, this.transform.position) < 15)
                    {
                        // slot into position
                        dragPosition = (Vector2)this.transform.position - tabletSlotOffset;
                        // (new Vector2(15 * Mathf.Sign(tabletSlotOffset.x), 15 * Mathf.Sign(tabletSlotOffset.y)) * ((spellSegment.shape.GetLength(0) - 3) % 2));
                    }
                    occupiedSlots = slots;
                    anchorPoint = this.GetComponent<RectTransform>().anchoredPosition;
                    this.shrunk = false;
                    AppendToInputSegments();
                }
            }
        }
        else
        {
            List<RaycastResult> results = RaycastMouse();
            if (results.Count < 1 || results[0].gameObject.tag != "Inventory")
            {
                Slot invSlot = inventoryManager.GetNextInventory();
                if (invSlot != null)
                {
                    invSlot.isOccupied = true;
                    occupiedSlots.Add(invSlot);
                    anchorPoint = invSlot.GetComponent<RectTransform>().anchoredPosition;
                }
            }
            this.shrunk = true;
        }

        currentPosition = anchorPoint;
        canvasGroup.blocksRaycasts = true;
        foreach (Transform child in this.transform) child.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = (child.tag == "Item" || child.tag == "Output") ? true : false;
        if (shrunk) this.ShrinkForInventory();
        dragging = false;
        // set the inventory positions
    }

    void Start()
    {
        this.inventoryManager = GameObject.Find("Inventory").GetComponent<InventoryManager>();
        this.canvasGroup = GetComponent<CanvasGroup>();
        this.GetComponent<RectTransform>().anchoredPosition = currentPosition;
        // Generate what this should look like instead
        CreateSpellSegmentPiece();
        int width = spellSegment.shape.GetLength(0) - 1;
        vectorOffSet = new Vector2(-((width % 2) * 15), (width % 2) * 15);
    }

    // Update is called once per frame
    void Update()
    {

        if (currentPosition != null && !dragging)
        {
            this.GetComponent<RectTransform>().anchoredPosition = Vector2.MoveTowards(this.GetComponent<RectTransform>().anchoredPosition, currentPosition,
                                                                                        0.4f + (Vector2.Distance(this.transform.position, currentPosition) / 20));
        }
        else if (dragging)
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, dragPosition, 4.0f + (Vector2.Distance(this.transform.position, dragPosition) / 5));
            if (Input.GetKeyDown(KeyCode.R) && rotating == 0)
            {
                rotating = 9;
                this.spellSegment.RotateSegment();
            }
        }

        if (rotating > 0)
        {
            RotateStep();
        }
    }

    private void RotateStep()
    {
        this.transform.Rotate(new Vector3(0, 0, -10));
        rotating--;
    }

    /// <summary>
    ///  Shrinks the item when in an inventory slot
    /// </summary>
    public void ShrinkForInventory()
    {
        float maxSideLength = (spellSegment.shape.GetLength(1) > spellSegment.shape.GetLength(0)) ? spellSegment.shape.GetLength(1) : spellSegment.shape.GetLength(0);
        this.transform.localScale = new Vector3(0.80f / (maxSideLength - 2), 0.80f / (maxSideLength - 2), 0.80f / (maxSideLength - 2));
    }

    /// <summary>
    ///  Gets all raycast results of objects under mouse
    /// </summary>
    public List<RaycastResult> RaycastMouse()
    {

        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };

        pointerData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        return results;
    }

    /// <summary>
    ///Generates the piece from spell segment variable
    /// </summary>
    public void CreateSpellSegmentPiece()
    {
        float size = solidImage.gameObject.GetComponent<RectTransform>().rect.width;
        int width = spellSegment.shape.GetLength(0) - 1;
        int height = spellSegment.shape.GetLength(1) - 1;

        for (int i = 0; i < width + 1; i++)
        {
            for (int j = 0; j < height + 1; j++)
            {
                SpellSegment.Segment segment = spellSegment.shape[i, j];
                if (segment != SpellSegment.Segment.Empty)
                {
                    if (segment == SpellSegment.Segment.Solid)
                    {
                        GameObject itemImage = Instantiate(solidImage);
                        itemImage.transform.SetParent(this.transform, false);
                        itemImage.transform.SetAsFirstSibling();
                        RectTransform imageRect = itemImage.GetComponent<RectTransform>();
                        imageRect.localScale.Set(1, 1, 1);
                        itemImage.transform.localPosition = Vector3.zero +
                            new Vector3(itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + i), itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + j))
                            - new Vector3(-((width % 2) * 15), (width % 2) * 15);
                        itemImage.transform.localPosition += new Vector3(-itemImage.GetComponent<RectTransform>().rect.width, 0) * ((spellSegment.shape.GetLength(0) + 1) % 2);
                        this.solidBlockObjects.Add(itemImage);
                    }
                    else
                    {
                        GameObject itemImage = Instantiate(inputOutputImage);
                        itemImage.transform.SetParent(this.transform, false);
                        itemImage.transform.SetAsFirstSibling();
                        RectTransform imageRect = itemImage.GetComponent<RectTransform>();

                        imageRect.localScale.Set(1, 1, 1);
                        itemImage.transform.localPosition = Vector3.zero +
                            new Vector3(itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + i), itemImage.GetComponent<RectTransform>().rect.width * (-(width / 2) + j))
                            - new Vector3(-((width % 2) * 15), (width % 2) * 15);
                        itemImage.transform.localPosition += new Vector3(-itemImage.GetComponent<RectTransform>().rect.width, 0) * ((spellSegment.shape.GetLength(0) + 1) % 2);


                        if (segment == SpellSegment.Segment.InputRight)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            this.inputObjectPairs.Add(itemImage, SpellSegment.Segment.InputRight);
                        }
                        else if (segment == SpellSegment.Segment.InputDown)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            itemImage.transform.Rotate(0, 0, -90);
                            this.inputObjectPairs.Add(itemImage, SpellSegment.Segment.InputDown);
                        }
                        else if (segment == SpellSegment.Segment.InputLeft)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.transform.Rotate(0, 0, 180);
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            this.inputObjectPairs.Add(itemImage, SpellSegment.Segment.InputLeft);
                        }
                        else if (segment == SpellSegment.Segment.InputUp)
                        {
                            itemImage.GetComponent<Image>().color = Color.cyan;
                            itemImage.transform.Rotate(0, 0, 90);
                            itemImage.GetComponent<CanvasGroup>().alpha = 0.4f;
                            this.inputObjectPairs.Add(itemImage, SpellSegment.Segment.InputUp);
                        }
                        else if (segment == SpellSegment.Segment.OutputRight)
                        {
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.tag = "Output";
                            this.outputObjects.Add(itemImage);
                        }
                        else if (segment == SpellSegment.Segment.OutputDown)
                        {
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.transform.Rotate(0, 0, -90);
                            itemImage.tag = "Output";
                            this.outputObjects.Add(itemImage);
                        }
                        else if (segment == SpellSegment.Segment.OutputLeft)
                        {
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.transform.Rotate(0, 0, 180);
                            itemImage.tag = "Output";
                            this.outputObjects.Add(itemImage);
                        }
                        else if (segment == SpellSegment.Segment.OutputUp)
                        {
                            itemImage.GetComponent<Image>().color = Color.red;
                            itemImage.GetComponent<CanvasGroup>().blocksRaycasts = true;
                            itemImage.transform.Rotate(0, 0, 90);
                            itemImage.tag = "Output";
                            this.outputObjects.Add(itemImage);
                        }

                    }
                }
                /*
                if (segment == SpellSegment.Segment.InputRight)
                {
                    GameObject itemImage = Instantiate(inputImage);
                    itemImage.transform.SetParent(this.transform, false);
                    RectTransform imageRect = itemImage.GetComponent<RectTransform>();
                    imageRect.localScale.Set(1, 1, 1);
                    itemImage.transform.Rotate(0,0,90);
                    itemImage.transform.localPosition = Vector3.zero +
                        new Vector3(size * (-(width / 2) + i), size * (-(width / 2) + j))
                        - new Vector3(-((width % 2) * 15), (width % 2) * 15)
                        + new Vector3(0 , (size / 2) + (imageRect.rect.width / 2));
                    itemImage.transform.localPosition += new Vector3(-size, 0) * ((spellSegment.shape.GetLength(0) + 1) % 2);
                }

                if (segment == SpellSegment.Segment.OutputRight)
                {
                    GameObject itemImage = Instantiate(outputImage);
                    itemImage.transform.SetParent(this.transform, false);
                    RectTransform imageRect = itemImage.GetComponent<RectTransform>();
                    imageRect.localScale.Set(1, 1, 1);
                    itemImage.transform.Rotate(0, 0, 90);
                    itemImage.transform.localPosition = Vector3.zero +
                        new Vector3(size * (-(width / 2) + i), size * (-(width / 2) + j))
                        - new Vector3(-((width % 2) * 15), (width % 2) * 15)
                        - new Vector3(0, (size / 2) + (imageRect.rect.width / 2));
                    itemImage.transform.localPosition += new Vector3(-size, 0) * ((spellSegment.shape.GetLength(0) + 1) % 2);
                }
                */
            }
        }
    }

    // returns the distance from a slot to a segment piece, else returns negative infinity
    public Vector2 CheckSlotPositions()
    {
        Vector2 anchorPos = this.GetComponent<RectTransform>().anchoredPosition;
        Vector2 directionOffset = Vector2.zero;
        List<GameObject> blockPositions = new List<GameObject>();
        blockPositions.AddRange(solidBlockObjects);
        blockPositions.AddRange(outputObjects);

        foreach (GameObject rayObj in blockPositions)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            pointerData.position = (Vector2)rayObj.transform.position;

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);
            if (results.Count != 1)
            {
                return Vector2.negativeInfinity;
            }
            foreach (RaycastResult result in results)
            {

                if (result.gameObject.tag == "Tablet")
                {
                    float distance = Vector2.Distance(result.gameObject.transform.position, pointerData.position);
                    if (distance < Mathf.Abs((result.gameObject.transform as RectTransform).rect.x / 1.3f))
                    {
                        directionOffset = pointerData.position - (Vector2)result.gameObject.transform.position;
                    }
                    else
                    {
                        return Vector2.negativeInfinity;
                    }
                }
            }
        }
        return directionOffset;
    }

    public bool CheckInputSlots()
    {
        if (inputObjectPairs.Count == 0)
        {
            return true;
        }

        foreach (KeyValuePair<GameObject, SpellSegment.Segment> valPair in inputObjectPairs)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            pointerData.position = (Vector2)(valPair.Key.transform.position);

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            foreach (RaycastResult result in results)
            {

                if (result.gameObject.tag == "Output")
                {
                    if (CheckSlotIn(result.gameObject.transform.rotation.eulerAngles.z, valPair.Value))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public List<Item> GetInputSpellSegments()
    {
        List<Item> inputSegmentList = new List<Item>();

        foreach (KeyValuePair<GameObject, SpellSegment.Segment> valPair in inputObjectPairs)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            pointerData.position = (Vector2)valPair.Key.transform.position;

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            foreach (RaycastResult result in results)
            {
                if (result.gameObject.tag == "Output")
                {
                    inputSegmentList.Add(result.gameObject.transform.parent.GetComponent<Item>());
                }
            }
        }
        return inputSegmentList;
    }

    public void AppendToInputSegments()
    {
        List<Item> inputItems = GetInputSpellSegments();

        foreach (Item item in inputItems)
        {
            this.inputSegments.Add(item);
            item.spellSegment.followingSegments.Add(this.spellSegment);
            if (!item.outputSegments.Contains(this))
            {
                item.outputSegments.Add(this);
            }
        }
    }

    //removes self from the input segment item, also removes
    public void RemoveFromInputSegments()
    {
        //Clear slots underneath
        foreach (Slot slot in occupiedSlots)
        {
            slot.isOccupied = false;
        }
        if (inventoryManager.starterSegments.Contains(this.spellSegment)) inventoryManager.starterSegments.Remove(this.spellSegment);
        this.occupiedSlots.Clear();
        List<Item> inputSegmentList = this.GetInputSpellSegments();
        foreach (Item inputItem in inputSegmentList)
        {
            inputItem.spellSegment.followingSegments.Remove(this.spellSegment);
        }
        // remove following segments 
        this.RemoveOutputItems();
    }

    // Removes each following segment and adds it to the inventory  
    public void RemoveOutputItems()
    {
        if (!this.dragging)
        {
            Slot invSlot = inventoryManager.GetNextInventory();
            if (invSlot != null)
            {
                invSlot.isOccupied = true;
                occupiedSlots.Add(invSlot);
                currentPosition = invSlot.GetComponent<RectTransform>().anchoredPosition;
                anchorPoint = invSlot.GetComponent<RectTransform>().anchoredPosition;
                this.ShrinkForInventory();
            }
        }
        this.inputSegments.Clear();

        while (outputSegments.Count > 0)
        {
            outputSegments[0].RemoveOutputItems();
            this.spellSegment.followingSegments.Clear();
            outputSegments.RemoveAt(0);
        }
    }

    public void SetAnchorPoint(Vector2 pos)
    {
        anchorPoint = pos + (Vector2)vectorOffSet;
    }

    public Vector2 PiecePositionBasedOnRotation(Vector2 piece)
    {
        int sign = (int)Mathf.Sign(transform.rotation.eulerAngles.z);
        int rotation = (int)Mathf.Abs(transform.rotation.eulerAngles.z) % 360;
        int xPosSign = piece.x > 0 ? -1 : 1;
        int yPosSign = piece.y > 50 ? -1 : 1;
        if (rotation == 90)
        {
            return sign * new Vector2(piece.y * -yPosSign, piece.x * xPosSign);
        }
        else if (rotation == 180)
        {
            return new Vector2(-piece.x, -piece.y);
        }
        else if (rotation == 270)
        {
            return sign * new Vector2(piece.y * yPosSign, piece.x * -xPosSign);
        }
        else
        {
            return piece;
        }
    }

    public List<Slot> GetSlotsUnderSegment()
    {
        List<Slot> slots = new List<Slot>();
        Vector2 anchorPos = this.GetComponent<RectTransform>().anchoredPosition;
        Vector2 directionOffset = Vector2.zero;
        List<GameObject> blockPositions = new List<GameObject>();
        blockPositions.AddRange(solidBlockObjects);
        blockPositions.AddRange(outputObjects);
        foreach (GameObject rayObj in blockPositions)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            pointerData.position = (Vector2)rayObj.transform.position;

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            foreach (RaycastResult result in results)
            {

                if (result.gameObject.tag == "Tablet")
                {
                    slots.Add(result.gameObject.GetComponent<Slot>());
                }
            }
        }
        return slots;
    }

    // Checks if the input matches the output, rotation 0 = right, rotation 180 = left
    public bool CheckSlotIn(float rotation, SpellSegment.Segment segment)
    {
        int calibratedRotation = (int)((Mathf.FloorToInt(rotation) + 90) % 360);

        int objRotation = (int)(Mathf.FloorToInt(this.transform.rotation.eulerAngles.z));

        if (segment == SpellSegment.Segment.InputRight)
        {
            objRotation = ((objRotation + 90) % 360);
        }
        else if (segment == SpellSegment.Segment.InputUp)
        {
            objRotation = ((objRotation + 180) % 360);
        }
        else if (segment == SpellSegment.Segment.InputLeft)
        {
            objRotation = ((objRotation + 270) % 360);
        }

        if (calibratedRotation == objRotation)
        {
            return true;
        }

        return false;
    }
}
