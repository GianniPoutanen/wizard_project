﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsLibrary
{
    public List<SpellSegment> availableSegments = new List<SpellSegment>();
    public List<SpellSegment> availableStarters = new List<SpellSegment>();

    // returns a random segment in availableSegments
    public SpellSegment GetRandomSegment()
    {
        return availableSegments[Mathf.FloorToInt(Random.Range(0, (availableSegments.Count - 0.0000000000001f)))];
    }

    public void PopulateStarters()
    {
        SpellSegment segment;

        // Starter segments

        segment = new SpellSegment();
        segment.starter = true;
        segment.numProjectiles = 1;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.OutputUp, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty} };
        availableStarters.Add(segment);
    }

    public void PopulateSegments()
    {
        SpellSegment segment;

        //List of all spell segments in game
        /*
        for ease copy template
        
        segment = new SpellSegment();
        // changes here
        availableSegments.Add(segment);

        // copy one
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.OutputUp, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty} };
        
         
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty,  SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.Solid, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.OutputRight, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty}};
         */

        // Damage Increase Segments

        segment = new SpellSegment();
        segment.damageChange = 1;
        segment.sizeChange = 0.01f;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.OutputUp, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty} };
        availableSegments.Add(segment);

        segment = new SpellSegment();
        segment.damageChange = 3;
        segment.sizeChange = 0.4f;
        segment.speedChange = -0.3f;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty,  SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.Solid, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.OutputRight, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty}};
        availableSegments.Add(segment);

        segment = new SpellSegment();
        segment.damageChange = 5;
        segment.sizeChange = 0.8f;
        segment.speedChange = -1f;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty,  SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.Solid, SpellSegment.Segment.OutputLeft, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Solid, SpellSegment.Segment.Solid, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty}};
        availableSegments.Add(segment);


        // Speed Increase Segments

        segment = new SpellSegment();
        segment.damageChange = 1;
        segment.sizeChange = 0.01f;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.OutputUp, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty} };
        availableSegments.Add(segment);

        segment = new SpellSegment();
        segment.damageChange = 3;
        segment.sizeChange = 0.4f;
        segment.speedChange = -0.3f;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty,  SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.Solid, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Solid, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty}};
        availableSegments.Add(segment);

        segment = new SpellSegment();
        segment.damageChange = 5;
        segment.sizeChange = 0.8f;
        segment.speedChange = -1f;
        segment.shape = new SpellSegment.Segment[,] { { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty,  SpellSegment.Segment.Empty },
                                                      { SpellSegment.Segment.InputUp, SpellSegment.Segment.Solid, SpellSegment.Segment.OutputLeft, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Solid, SpellSegment.Segment.Solid, SpellSegment.Segment.Empty},
                                                      { SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty, SpellSegment.Segment.Empty}};
        availableSegments.Add(segment);

    }

    /// <summary>
    /// Generates a random segment with no key words
    /// </summary>
    /// <param name="randMax"> Maximum number of solids</param>
    /// <returns></returns>
    public SpellSegment GenerateRandomBasicSegment(int randMax)
    {
        int numSolids = Mathf.RoundToInt(Random.Range(0, randMax));
        SpellSegment segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, numSolids, 1);
        for (int i = 0; i < numSolids + 1; i++)
        {
            ChangeRandomStats(segment);
        }
        return segment;
    }
    public SpellSegment GenerateRandomSegmentWithKeyWords(List<SpellSegment.KeyWord> keyWords)
    {
        int numSolids = 2;
        SpellSegment segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, numSolids, 1);
        for (int i = 0; i < numSolids + 1; i++)
        {
            ChangeRandomStats(segment);
        }
        return segment;
    }

    public SpellSegment GenerateRandomStarter()
    {
        SpellSegment segment = new SpellSegment();
        return segment;
    }
    public SpellSegment GenerateRandomConnector()
    {
        SpellSegment segment = new SpellSegment();
        return segment;
    }
    public SpellSegment GenerateRandomFinisher()
    {
        SpellSegment segment = new SpellSegment();
        return segment;
    }

    public List<SpellSegment> GenerateAllKeyWordSegents()
    {
        List<SpellSegment> keyWordSegments = new List<SpellSegment>();
        SpellSegment segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, 0, 1);
        segment.segmentKeyWords.Add(SpellSegment.KeyWord.Split);
        keyWordSegments.Add(segment);
        segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, 0, 1);
        segment.chargeTimeChange = -1;
        keyWordSegments.Add(segment);
        /*
        segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, 0, 1);
        segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitEvenly);
        keyWordSegments.Add(segment);
        segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, 0, 3);
        segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitForward);
        segment.numProjectiles = 2;
        keyWordSegments.Add(segment);
        segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, 0, 1);
        segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitRandom);
        keyWordSegments.Add(segment);
        segment = new SpellSegment();
        segment.shape = ShapeGenerator.GenerateShape(1, 0, 1);
        segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitRandomForward);
        keyWordSegments.Add(segment);*/
        return keyWordSegments;
    }

    public void AddPositiveToRandomStats(SpellSegment segment)
    {
        int randInt = Mathf.FloorToInt(Random.Range(0, 5.99999f));

        switch (randInt)
        {
            case 0:
                segment.damageChange += (0.5f * Mathf.RoundToInt(Random.Range(1, 5)));
                break;
            case 1:
                segment.rangeChange += (0.4f * Mathf.RoundToInt(Random.Range(1, 5)));
                break;
            case 2:
                segment.accuracyChange -= (3f * Mathf.RoundToInt(Random.Range(1, 5)));
                break;
            case 3:
                segment.speedChange += (0.3f * Mathf.RoundToInt(Random.Range(1, 5)));
                break;
            case 4:
                segment.chargeTimeChange -= (0.05f * Mathf.RoundToInt(Random.Range(1, 5)));
                break;
            case 5:
                segment.sizeChange += (0.01f * Mathf.RoundToInt(Random.Range(1, 5)));
                break;
        }
    }
    public void AddNegativeToRandomStats(SpellSegment segment)
    {
        int randInt = Mathf.FloorToInt(Random.Range(0, 5.99999f));

        switch (randInt)
        {
            case 0:
                segment.damageChange -= (0.5f * Mathf.FloorToInt(Random.Range(0, 2.9999f)));
                break;
            case 1:
                segment.rangeChange -= (0.2f * Mathf.FloorToInt(Random.Range(0, 2.9999f)));
                break;
            case 2:
                segment.accuracyChange += (0.2f * Mathf.FloorToInt(Random.Range(0, 2.9999f)));
                break;
            case 3:
                segment.speedChange -= (0.2f * Mathf.FloorToInt(Random.Range(0, 2.9999f)));
                break;
            case 4:
                segment.chargeTimeChange += (0.1f * Mathf.FloorToInt(Random.Range(0, 2.9999f)));
                break;
            case 5:
                segment.sizeChange -= (0.25f * Mathf.FloorToInt(Random.Range(0, 2.9999f)));
                break;
        }
    }

    // Shuffles random stats in ways that make sense 
    public void ChangeRandomStats(SpellSegment segment)
    {
        int randInt = Mathf.RoundToInt(Random.Range(0, 4.4f));
        int changeVal = Mathf.RoundToInt(Random.Range(0, 5f));
        switch (randInt)
        {
            case 0:
                segment.damageChange += 0.5f * changeVal;
                segment.sizeChange += 0.025f * changeVal / 2;
                segment.speedChange -= 0.1f * (changeVal - 1);
                break;
            case 1:
                segment.rangeChange += 0.4f * changeVal;
                segment.speedChange += 0.2f * changeVal;
                segment.accuracyChange += 1f * (changeVal - 1);
                break;
            case 2:
                segment.accuracyChange -= 3f * changeVal;
                segment.speedChange += 0.05f * (changeVal - 1);
                segment.damageChange -= 0.25f * (changeVal - 1);
                break;
            case 3:
                segment.speedChange += 0.3f * changeVal;
                segment.damageChange += 0.25f * (changeVal - 1);
                segment.accuracyChange += 1f * changeVal;
                break;
            case 4:
                segment.chargeTimeChange -= 0.3f * changeVal;
                segment.accuracyChange += 0.15f * changeVal;
                break;
        }
    }

    public void AddRandomKeyWord(SpellSegment segment)
    {
        bool addedKeyword = false;
        while (!addedKeyword)
        {
            int randInt = Mathf.RoundToInt(Random.Range(0, 4.4f));
            switch (randInt)
            {
                case 0:
                    if (!(segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitEvenly) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitForward)
                        || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandom) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandomForward)))
                    {
                        if (segment.segmentKeyWords.Contains(SpellSegment.KeyWord.Split))
                            segment.numProjectiles++;
                        else
                            segment.segmentKeyWords.Add(SpellSegment.KeyWord.Split);
                        addedKeyword = true;
                    }
                    break;
                case 1:
                    if (!(segment.segmentKeyWords.Contains(SpellSegment.KeyWord.Split) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitForward)
                        || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandom) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandomForward)))
                    {
                        if (segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitEvenly))
                            segment.numProjectiles++;
                        else
                            segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitEvenly);
                        addedKeyword = true;
                    }
                    break;
                case 2:
                    if (!(segment.segmentKeyWords.Contains(SpellSegment.KeyWord.Split) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitEvenly)
                        || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandom) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandomForward)))
                    {
                        if (segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitForward))
                            segment.numProjectiles++;
                        else
                            segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitForward);
                        addedKeyword = true;
                    }
                    break;
                case 3:
                    if (!(segment.segmentKeyWords.Contains(SpellSegment.KeyWord.Split) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitEvenly)
                        || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitForward) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandomForward)))
                    {
                        if (segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandom))
                            segment.numProjectiles++;
                        else
                            segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitRandom);
                        addedKeyword = true;
                    }
                    break;
                case 4:
                    if (!(segment.segmentKeyWords.Contains(SpellSegment.KeyWord.Split) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitEvenly)
                        || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitForward) || segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandom)))
                    {
                        if (segment.segmentKeyWords.Contains(SpellSegment.KeyWord.SplitRandomForward))
                            segment.numProjectiles++;
                        else
                            segment.segmentKeyWords.Add(SpellSegment.KeyWord.SplitRandomForward);
                        addedKeyword = true;
                    }
                    break;
            }
        }
    }
}
