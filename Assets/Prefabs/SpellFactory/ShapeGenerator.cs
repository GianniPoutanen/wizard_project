﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeGenerator
{
    // Generates a random shape with input and output
    public static SpellSegment.Segment[,] GenerateShape(int numInput, int numSolid, int numOutput)
    {
        List<int[]> solidPositions = new List<int[]>();
        int shapeSize = numSolid + numOutput;
        int arraySize = Mathf.CeilToInt(shapeSize / 2) + 1;
        SpellSegment.Segment[,] shape = new SpellSegment.Segment[arraySize, arraySize];

        // Init shape to empty
        for (int i = 0; i < arraySize; i++)
        {
            for (int j = 0; j < arraySize; j++)
            {
                shape[i, j] = SpellSegment.Segment.Empty;
            }
        }

        // Init start solid position
        int[] randPos = new int[] { Mathf.RoundToInt(Random.Range(0, arraySize - 1)), Mathf.RoundToInt(Random.Range(0, arraySize - 1)) };
        shape[randPos[0], randPos[1]] = SpellSegment.Segment.Solid;
        solidPositions.Add(randPos);

        // generate random shape of solid and add to shape
        while (solidPositions.Count < shapeSize)
        {
            for (int i = 0; i < arraySize; i++)
            {
                for (int j = 0; j < arraySize; j++)
                {
                    if (HasNeighbour(i, j, shape) && WeightedRandom(i, j, arraySize) && solidPositions.Count != shapeSize)
                    {
                        shape[i, j] = SpellSegment.Segment.Solid;
                        solidPositions.Add(new int[] { i, j });
                    }
                }
            }
        }

        while (numOutput > 0)
        {
            int randSolid = Mathf.RoundToInt(Random.Range(0, solidPositions.Count - 1));
            int[] solidPos = solidPositions[randSolid];
            int[] randPosition = GetEmptyNeighbour(solidPos[0], solidPos[1], shape);
            if (randPosition != null)
            {
                if (solidPos[0] > randPosition[0])
                {
                    shape[solidPos[0], solidPos[1]] = SpellSegment.Segment.OutputLeft;
                }
                else if (solidPos[0] < randPosition[0])
                {
                    shape[solidPos[0], solidPos[1]] = SpellSegment.Segment.OutputRight;
                }
                else if (solidPos[1] > randPosition[1])
                {
                    shape[solidPos[0], solidPos[1]] = SpellSegment.Segment.OutputDown;
                }
                else if (solidPos[1] < randPosition[1])
                {
                    shape[solidPos[0], solidPos[1]] = SpellSegment.Segment.OutputUp;
                }
                numOutput--;
            }
            else
            {
                randSolid = (randSolid + 1) % arraySize;
            }
        }

        SpellSegment.Segment[,] finalShape = new SpellSegment.Segment[arraySize + 2, arraySize + 2];
        // Init shape to empty
        for (int i = 0; i < arraySize + 2; i++)
        {
            for (int j = 0; j < arraySize + 2; j++)
            {
                if (i - 1 > -1 && i - 1 < (arraySize) && j - 1 > -1 && j - 1 < (arraySize))
                {
                    finalShape[i, j] = shape[i - 1, j - 1];
                }
                else
                {
                    finalShape[i, j] = SpellSegment.Segment.Empty;
                }
            }
        }

        while (numInput > 0)
        {
            int randSolid = Mathf.RoundToInt(Random.Range(0, solidPositions.Count));
            int[] solidPos = new int[] { solidPositions[randSolid][0] + 1, solidPositions[randSolid][1] + 1 };
            int[] randPosition = GetEmptyNeighbour(solidPos[0], solidPos[1], finalShape);
            if (randPosition != null)
            {
                if (solidPos[0] > randPosition[0] && finalShape[solidPos[0], solidPos[1]] != SpellSegment.Segment.OutputLeft)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellSegment.Segment.InputRight;
                    numInput--;
                }
                else if (solidPos[0] < randPosition[0] && finalShape[solidPos[0], solidPos[1]] != SpellSegment.Segment.OutputRight)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellSegment.Segment.InputLeft;
                    numInput--;
                }
                else if (solidPos[1] > randPosition[1] && finalShape[solidPos[0], solidPos[1]] != SpellSegment.Segment.OutputDown)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellSegment.Segment.InputUp;
                    numInput--;
                }
                else if (solidPos[1] < randPosition[1] && finalShape[solidPos[0], solidPos[1]] != SpellSegment.Segment.OutputUp)
                {
                    finalShape[randPosition[0], randPosition[1]] = SpellSegment.Segment.InputDown;
                    numInput--;
                }
            }
        }

        return finalShape;
    }

    public static bool HasNeighbour(int x, int y, SpellSegment.Segment[,] shape)
    {
        if ((x + 1) < shape.GetLength(0) && shape[x + 1, y] == SpellSegment.Segment.Solid ||
            (x - 1) > -1 && shape[x - 1, y] == SpellSegment.Segment.Solid ||
            (y + 1) < shape.GetLength(0) && shape[x, y + 1] == SpellSegment.Segment.Solid ||
            (y - 1) > -1 && shape[x, y - 1] == SpellSegment.Segment.Solid)
        {
            return true;
        }
        return false;
    }

    public static bool WeightedRandom(int x, int y, int size)
    {
        Vector2 point = new Vector2(x, y);
        Vector2 midPoint = new Vector2(size / 2, size / 2);
        float randomVal = Random.Range(0, (size / 2) + 0.5f);
        float weightedRange = Vector2.Distance(point, midPoint);
        return randomVal > weightedRange;
    }

    public static int[] GetEmptyNeighbour(int x, int y, SpellSegment.Segment[,] shape)
    {
        List<int[]> emptySpaces = new List<int[]>();
        if ((x + 1) < shape.GetLength(0) && shape[x + 1, y] == SpellSegment.Segment.Empty || x + 1 == shape.GetLength(0))
            emptySpaces.Add(new int[] { x + 1, y });
        if ((x - 1) > -1 && shape[x - 1, y] == SpellSegment.Segment.Empty || x - 1 == -1)
            emptySpaces.Add(new int[] { x - 1, y });
        if ((y + 1) < shape.GetLength(0) && shape[x, y + 1] == SpellSegment.Segment.Empty || y + 1 == shape.GetLength(0))
            emptySpaces.Add(new int[] { x, y + 1 });
        if ((y - 1) > -1 && shape[x, y - 1] == SpellSegment.Segment.Empty || y - 1 == -1)
            emptySpaces.Add(new int[] { x, y - 1 });

        if (emptySpaces.Count > 0)
        {
            return emptySpaces[Mathf.RoundToInt(Random.Range(0, emptySpaces.Count))];
        }

        return null;
    }
}
