﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
    public InventoryManager inventoryManager;
    public bool isInventory;
    public bool isOccupied;
    public int[] slotPosition;

    private void Start()
    {
        this.inventoryManager = GameObject.Find("Inventory").GetComponent<InventoryManager>();
        if (isInventory)
        {
            this.tag = "Inventory";
        } 
        else
        {
            this.tag = "Tablet";
        }
    }

    // Item dropped
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Dropped");
        if (eventData.pointerDrag != null && inventoryManager.ValidatePlacement(eventData.pointerDrag.GetComponent<Item>(), slotPosition) && eventData.pointerDrag.tag == "Item")
        {
            // change from anchor point to one of the shape positions
            if (isInventory)
            {
                // call the shrunk thing
                eventData.pointerDrag.GetComponent<Item>().ShrinkForInventory();
                eventData.pointerDrag.GetComponent<Item>().shrunk = true;
                this.isOccupied = true;
            } 
        }
    }
}
