﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class SpellSegment
{
    public enum KeyWord
    {
        Straight,
        Split, // splits and moves within 45 degrees of current direction
        SplitForward,
        SplitRandom,
        SplitRandomForward,
        SplitEvenly,
        Spray,
        Explode,
        Return,
        Wizzing,
        Burst,
        Far,
        DelayedSpeedUp,
    }
    //Keywords related constants
    public int splitConstant = 1;

    public List<KeyWord> segmentKeyWords = new List<KeyWord>();

    public enum Segment
    {
        Empty,
        Solid,
        InputRight,
        InputLeft,
        InputUp,
        InputDown,
        OutputRight,
        OutputLeft,
        OutputUp,
        OutputDown
    }
    [Header("Shape in inventory")]
    // 2 is solid position, 1 is in position, 3 is out position
    public Segment[,] shape = new Segment[,] { { Segment.Empty, Segment.Empty, Segment.Empty },
                                               { Segment.InputUp, Segment.OutputRight, Segment.Empty},
                                               { Segment.Empty, Segment.Empty, Segment.Empty } };

    [Header("Segment specific stats")]
    public string title = "";
    public SpellStats spellStats = new SpellStats();
    public int rangeRatio = 1;

    [Header("Changes in stats for entire spell")]
    public float accuracyChange = 0;
    public float sizeChange = 0;
    public float rangeChange = 0.1f;
    public float damageChange = 0.25f;
    public float speedChange = 0;
    public float chargeTimeChange = 0.2f;

    [Header("Changes in stats for transition in spell")]
    public float transitionDirectionChange = 0;
    public float transitionAccuracyChange = 0;
    public float transitionSizeChange = 0;
    public float transitionRangeChange = 0;
    public float transitionDamageChange = 0;
    public float transitionSpeedChange = 0;


    [HideInInspector]
    public bool prefabChange;
    [HideInInspector]
    public KeyWord castType;
    public GameObject changedPrefab;


    [Header("Variables for the start of spell")]
    public Vector3 spellStartPosition;
    public Vector3 spellNextPosition = Vector3.negativeInfinity; // look at split forward for an example
    public Vector3 spellEndPosition;
    public GameObject spell;

    // Spell casting stats
    public bool chargeRelease = false;

    public bool starter = false;


    [Header("Straight spell stats")]
    public int numProjectiles = 1; // number of projectiles fired
    public float projectileDelay = 0.2f; // time between projectile fire

    [Header("Following segments in the spell")]
    public List<SpellSegment> followingSegments = new List<SpellSegment>();

    #region Constructor
    public SpellSegment() { } // nothing required to instantiate
    // Creates a deep copy
    public SpellSegment(SpellSegment segment)
    {
        this.spellStats = segment.spellStats;
        this.followingSegments = segment.followingSegments;
    }
    #endregion
    #region Methods

    #region During Spell
    public void SpellTransitionEffect(GameObject gameObject)
    {
        // called at spell segment start
        gameObject.transform.rotation *= Quaternion.Euler(0, 0, Random.Range(-transitionDirectionChange, transitionDirectionChange));
    }
    public void SpellEffectStep()
    {
        // called in projectile update
    }
    // returns spell segment if new segment doesn't destyor, else null
    public SpellSegment SpellFinisher()
    {
        spellEndPosition = spell.transform.position;
        if (followingSegments.Count == 1)
        {
            followingSegments[0].spellStats = this.spellStats;
            followingSegments[0].spell = this.spell;
            if (followingSegments[0].segmentKeyWords.Contains(KeyWord.Split))
            {
                followingSegments[0].Split(spellEndPosition);
                return null;
            }
            else if (followingSegments[0].segmentKeyWords.Contains(KeyWord.SplitEvenly))
            {
                followingSegments[0].SplitEvenly(spellEndPosition);
                return null;
            }
            else if (followingSegments[0].segmentKeyWords.Contains(KeyWord.SplitRandomForward))
            {
                followingSegments[0].SplitRandomForward(spellEndPosition);
                return null;
            }
            else if (followingSegments[0].segmentKeyWords.Contains(KeyWord.SplitRandom))
            {
                followingSegments[0].SplitRandom(spellEndPosition);
                return null;
            }
            else if (followingSegments[0].segmentKeyWords.Contains(KeyWord.SplitForward))
            {
                followingSegments[0].SplitForward(spellEndPosition);
                return null;
            }
            return followingSegments[0];
        }
        else if (followingSegments.Count > 1)
        {
            Debug.Log("Create a smart split");
            // create a split
        }
        return null;
    }
    #endregion

    #region SplitFunctions

    public void Split(Vector3 pos)
    {
        int numSplits = numProjectiles + splitConstant;
        this.spellStats.damage = this.spellStats.damage / numSplits;
        this.spellStartPosition = pos;
        float rotationOffset = 3 * this.spellStats.accuracy;
        float rotationGap = rotationOffset / (numSplits - 1);
        for (int i = 0; i < numSplits; i++)
        {
            GameObject newProj = CastProjectile();
            newProj.transform.rotation = spell.transform.rotation;
            newProj.transform.eulerAngles += new Vector3(0, 0, -(rotationOffset / 2) + (rotationGap * i));
            newProj.GetComponent<ProjectileScript>().currentSegment.spell = newProj;
        }
    }

    public void SplitRandomForward(Vector3 pos)
    {
        int numSplits = numProjectiles + splitConstant;
        this.spellStats.damage = this.spellStats.damage / numSplits;
        this.spellStartPosition = pos;
        float rotationOffset = 3 * this.spellStats.accuracy;
        for (int i = 0; i < numSplits; i++)
        {
            GameObject newProj = CastProjectile();
            newProj.transform.rotation = spell.transform.rotation;
            newProj.transform.eulerAngles += new Vector3(0, 0, -(rotationOffset / 2) + Random.Range(0, rotationOffset));
            newProj.GetComponent<ProjectileScript>().currentSegment.spell = newProj;
        }
    }

    public void SplitEvenly(Vector3 pos)
    {
        int numSplits = numProjectiles + splitConstant;
        this.spellStats.damage = this.spellStats.damage / numSplits;
        this.spellStartPosition = pos;
        int rotationOffset = 360 / numSplits;
        for (int i = 0; i < numSplits; i++)
        {
            GameObject newProj = CastProjectile();
            newProj.transform.rotation = spell.transform.rotation;
            newProj.transform.eulerAngles += new Vector3(0, 0, 90 + (rotationOffset * i));
            newProj.GetComponent<ProjectileScript>().currentSegment.spell = newProj;
        }
    }

    public void SplitRandom(Vector3 pos)
    {
        int numSplits = numProjectiles + splitConstant;
        this.spellStats.damage = this.spellStats.damage / numSplits;
        this.spellStartPosition = pos;
        int rotationOffset = 360 / numSplits;
        for (int i = 0; i < numSplits; i++)
        {
            GameObject newProj = CastProjectile();
            newProj.transform.rotation = spell.transform.rotation;
            newProj.transform.eulerAngles += new Vector3(0, 0, Random.Range(0.0f,  360.0f));
            newProj.GetComponent<ProjectileScript>().currentSegment.spell = newProj;
        }
    }

    // TODO hard
    public void SplitForward(Vector3 pos)
    {
        int numSplits = numProjectiles + splitConstant;
        this.spellStats.damage = this.spellStats.damage / numSplits;
        this.spellStartPosition = pos;
        int rotationOffset = 360 / numSplits;
        for (int i = 0; i < numSplits; i++)
        {
            GameObject newProj = CastProjectile();
            newProj.transform.rotation = spell.transform.rotation;
            newProj.GetComponent<ProjectileScript>().currentSegment.spell = newProj;
            Vector3 nextPos = newProj.transform.position + newProj.transform.up * i;
            newProj.GetComponent<ProjectileScript>().currentSegment.spellNextPosition = nextPos;
        }
    }

    #endregion

    #region HelperFunctions
    // Called when scale increases
    public void ScaleIncreaseOverTime(GameObject gameObject)
    {
        Vector3 initScale = gameObject.transform.localScale;
        gameObject.transform.localScale = new Vector3(initScale.x + sizeChange, initScale.y + sizeChange, initScale.z);
    }

    // reotates the shape 90 degrees
    public void RotateSegment()
    {
        this.shape = RotateMatrix(this.shape, this.shape.GetLength(0));
    }

    // rotates the array to match in game object rotation
    static Segment[,] RotateMatrix(Segment[,] matrix, int n)
    {
        Segment[,] ret = new Segment[n, n];

        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                ret[i, j] = matrix[n - j - 1, i];
            }
        }

        return ret;
    }

    // casts the spell at spell start position
    public void CastSpell()
    {
        switch (this.castType)
        {
            case KeyWord.Spray:
                return;
            default:
                spell = CastProjectile();
                spell.transform.rotation = GetRotationTowardsMouse();
                return;
        }
    }

    public GameObject CastProjectile()
    {
        GameObject castProjectile = GameObject.Instantiate(GetProjectileType());
        castProjectile.transform.position = this.spellStartPosition;
        castProjectile.transform.Rotate(0, 0, Random.Range(-(this.spellStats.accuracy - this.accuracyChange), (this.spellStats.accuracy - this.accuracyChange)));
        castProjectile.GetComponent<ProjectileScript>().currentSegment = new SpellSegment(this);
        castProjectile.GetComponent<ProjectileScript>().currentSegment.spell = castProjectile;
        return castProjectile;
    }

    private GameObject GetProjectileType()
    {
        return AssetDatabase.LoadAssetAtPath<UnityEngine.GameObject>("Assets/Prefabs/FireBallProjectile/TestProjectile.prefab");
    }

    private Quaternion GetRotationTowardsMouse()
    {
        Vector3 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.spellStartPosition;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
    #endregion

    public void UpdateSpellStats()
    {
        spellStats = new SpellStats();
        List<SpellSegment> segments = new List<SpellSegment>();
        segments.Add(this);
        int numSegments = 1;
        while (segments.Count > 0)
        {
            foreach (SpellSegment followingSegment in segments[0].followingSegments)
            {
                numSegments++;
                segments.Add(followingSegment);
            }
            spellStats.range += segments[0].rangeChange;
            spellStats.damage += segments[0].damageChange;
            spellStats.accuracy += segments[0].accuracyChange;
            spellStats.size += segments[0].sizeChange;
            spellStats.speed += segments[0].speedChange;
            spellStats.chargeTime += segments[0].chargeTimeChange;
            segments.RemoveAt(0);
        }
        segments.Add(this);
        while (segments.Count > 0)
        {
            foreach (SpellSegment followingSegment in segments[0].followingSegments)
            {
                numSegments++;
                segments.Add(followingSegment);
            }
            segments[0].rangeRatio = 1 / numSegments;
            segments.RemoveAt(0);
        }

        // default the neg values to lowest values 
        if (spellStats.range < 0.15f) spellStats.range = 0.15f;
        if (spellStats.damage < 0.1f) spellStats.damage = 0.1f;
        if (spellStats.accuracy < 0) spellStats.accuracy = 0;
        if (spellStats.size < 0.1f) spellStats.size = 0.1f;
        if (spellStats.speed < 0.4f) spellStats.speed = 0.3f;
        if (spellStats.chargeTime < 0.02f) spellStats.chargeTime = 0.02f;

        Debug.Log("Spell updated! - Dmg: " + spellStats.damage + "  Range: " + spellStats.range +
                                "  Accuracy: " + spellStats.accuracy + "  Speed: " + spellStats.speed +
                                "  Size: " + spellStats.size + "  Charge Time: " + spellStats.chargeTime);
    }
    #endregion
}


