﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellStats
{
    // Default starting stats
    [Header("Current stats")]
    public float speed = 1f;
    public float damage = 0.5f;
    public float range = 0.5f;
    public float accuracy = 15.0f;
    public float size = 1;
    public float chargeTime = 0.5f;
    public SpellSegment currentSpellSegment;

}
