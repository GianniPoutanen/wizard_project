﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicBolt : MonoBehaviour
{
    public float Damage = 1;
    public float Speed = 1;
    public float Range = 1;

    private float distanceTravelled = 0;
    private Vector3 lastPosition;


    void Start()
    {
        lastPosition = transform.position;
    }


    void Update()
    {
        distanceTravelled += Vector3.Distance(transform.position, lastPosition);
        lastPosition = transform.position;
    }
}
