﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpellEmitter : MonoBehaviour
{
    List<SpellSegment> spells = new List<SpellSegment>();
    List<float> castTimers = new List<float>();
    List<List<int>> chargedSpells = new List<List<int>>();
    List<List<float>> delayTimers = new List<List<float>>();

    public GameManager gm;


    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.pause)
        {
            if (Input.GetMouseButtonDown(0))
            {
                for (int i = 0; i < spells.Count; i++)
                {
                    castTimers[i] = (Time.time + spells[i].spellStats.chargeTime);
                    chargedSpells[i].Clear();
                    delayTimers[i].Clear();
                }
            }

            if (Input.GetMouseButton(0))
            {
                for (int i = 0; i < spells.Count; i++)
                {
                    if (castTimers[i] < Time.time)
                    {
                        chargedSpells[i].Add(spells[i].numProjectiles);
                        delayTimers[i].Add(Time.time);
                        castTimers[i] = Time.time + spells[i].spellStats.chargeTime;
                    }
                }
            }

            for (int i = 0; i < spells.Count; i++)
            {
                for (int j = 0; j < chargedSpells[i].Count; j++)
                {
                    if (chargedSpells[i][j] > 0 && delayTimers[i][j] < Time.time)
                    {
                        spells[i].spellStartPosition = this.transform.position;
                        spells[i].CastSpell();
                        chargedSpells[i][j]--;
                        delayTimers[i][j] = Time.time + spells[i].projectileDelay;
                        if (chargedSpells[i][j] == 0)
                        {
                            chargedSpells[i].Remove(j);
                            delayTimers[i].Remove(j);
                            j--;
                        }
                    }
                }
            }
        }
    }

    public void UpdateSpell(List<SpellSegment> spellStarters)
    {
        this.spells.Clear();
        foreach (SpellSegment segment in spellStarters)
        {
            segment.UpdateSpellStats();
            this.spells.Add(segment);
        }
        castTimers.Clear();
        chargedSpells.Clear();
        delayTimers.Clear();
        for (int i = 0; i < spells.Count; i++)
        {
            castTimers.Add(Time.time + spells[i].spellStats.chargeTime);
            chargedSpells.Add(new List<int>());
            delayTimers.Add(new List<float>());
            chargedSpells[i].Add(0);
            delayTimers[i].Add(Time.time);
        }
    }
}


